package com.stock.mvc.dao.impl;

import java.util.List;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.stock.mvc.dao.IGenericDao;


@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements IGenericDao<E> {
	
	@PersistenceContext
	EntityManager em;
	
	private Class<E> type;
	
	public GenericDaoImpl() {
		
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<E>) pt.getActualTypeArguments()[0];
	}

	@Override
	public E save(E entity) {
		
		em.persist(entity);
		return entity;
		
	}

	@Override
	public E update(E entity) {
		
		return em.merge(entity);
		
	}

	@Override
	public List<E> selectAll() {
		
		String qlString = "SELECT t FROM "+type.getSimpleName()+" t ";
		Query query = em.createQuery(qlString);
		return query.getResultList();
		
	}

	@Override
	public List<E> selectAll(String sortField, String sort) {
		
		String qlString = "SELECT t FROM "+type.getSimpleName()+" t ORDER BY "+sortField+" "+sort;
		Query query = em.createQuery(qlString);
		return query.getResultList();
		
	}

	@Override
	public E getById(Long id) {
		
		return em.find(type, id);
		
	}

	@Override
	public void remove(Long id) {
		
		E tab = em.getReference(type, id);
		em.remove(tab);
		
	}

	@Override
	public E findOne(String paraName, Object ParamValue) {
		
		String qlString = "SELECT t FROM "+type.getSimpleName()+" t WHERE "+paraName+" =:x";
		Query query = em.createQuery(qlString);
		query.setParameter(paraName, ParamValue);
		return query.getResultList().size() > 0 ? (E)query.getResultList().get(0) : null;
	}

	@Override
	public E findOne(String[] paraNames, Object[] ParamValues) {
		
		if(paraNames.length != ParamValues.length) return null ;
		
		String qlString = "SELECT e FROM "+type.getSimpleName()+" e WHERE ";
		
		int lenPn = paraNames.length;
		int lenPv = ParamValues.length;
		
		for (int i = 0; i < lenPn; i++) {
			
			qlString += " e."+ParamValues[i]+"= :x" + i;
			
			if ((i+1)< lenPn) {
				
				qlString += " and ";
				
			}

		}
		
		Query query = em.createQuery(qlString);
		
		for (int i = 0; i < lenPv; i++) {
			
			query.getParameter("x" + i + ParamValues[i]);
			
		}
		
		return query.getResultList().size() > 0 ? (E)query.getResultList().get(0) : null;
	}

	@Override
	public int findCountBy(String paraName, String ParamValue) {
		
		String qlString = "SELECT t FROM "+type.getSimpleName()+" t WHERE "+paraName+" =:x";
		Query query = em.createQuery(qlString);
		query.setParameter(paraName, ParamValue);
		return query.getResultList().size() > 0 ? ((Long)query.getSingleResult()).intValue() : 0;
	}

	public Class<E> getType() {
		return type;
	}

	
}
