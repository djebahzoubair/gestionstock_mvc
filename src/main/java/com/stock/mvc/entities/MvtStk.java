package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="mvtStk")
public class MvtStk implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue
	private Long idMvtStk;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvtStk;
	
	private BigDecimal quantite;
	
	private int typeMvtStk;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	
	public Long getIdMvtStk() {
		return idMvtStk;
	}


	public void setIdMvtStk(Long idMvtStk) {
		this.idMvtStk = idMvtStk;
	}


	public Date getDateMvtStk() {
		return dateMvtStk;
	}


	public void setDateMvtStk(Date dateMvtStk) {
		this.dateMvtStk = dateMvtStk;
	}


	public BigDecimal getQuantite() {
		return quantite;
	}


	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}


	public int getTypeMvtStk() {
		return typeMvtStk;
	}


	public void setTypeMvtStk(int typeMvtStk) {
		this.typeMvtStk = typeMvtStk;
	}


	public Article getArticle() {
		return article;
	}


	public void setArticle(Article article) {
		this.article = article;
	}

	
}
