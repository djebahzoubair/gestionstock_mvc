package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.Vente;

public interface IVenteService {
	
public Vente save (Vente entity);
	
	public Vente update (Vente entity);
	
	public List<Vente> selectAll();
	
	public List<Vente> selectAll(String sortField, String sort);
	
	public Vente getById(Long id);
	
	public void remove(Long id);
	
	public Vente findOne(String paraName, Object ParamValue );
	
	public Vente findOne(String paraNames[], Object ParamValues[] );
	
	public int findCountBy(String paraName, String ParamValue );

}
