package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IVenteDao;
import com.stock.mvc.entities.Vente;
import com.stock.mvc.services.IVenteService;


@Transactional
public class VenteServiceImpl implements IVenteService {
	
	private IVenteDao dao;
	
	public void setDao(IVenteDao dao) {
		this.dao = dao;
	}

	@Override
	public Vente save(Vente entity) {
		return dao.save(entity);
	}

	@Override
	public Vente update(Vente entity) {
		return dao.update(entity);
	}

	@Override
	public List<Vente> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Vente> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Vente getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Vente findOne(String paraName, Object ParamValue) {
		return dao.findOne(paraName, ParamValue);
	}

	@Override
	public Vente findOne(String[] paraNames, Object[] ParamValues) {
		return dao.findOne(paraNames, ParamValues);
	}

	@Override
	public int findCountBy(String paraName, String ParamValue) {
		return dao.findCountBy(paraName, ParamValue);
	}

}
