package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICategorieDao;
import com.stock.mvc.entities.Categorie;
import com.stock.mvc.services.ICategorieService;


@Transactional
public class CategorieServiceImpl implements ICategorieService {
	
	private ICategorieDao dao;
	
	public void setDao(ICategorieDao dao) {
		this.dao = dao;
	}

	@Override
	public Categorie save(Categorie entity) {
		return dao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		return dao.update(entity);
	}

	@Override
	public List<Categorie> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Categorie> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Categorie getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Categorie findOne(String paraName, Object ParamValue) {
		return dao.findOne(paraName, ParamValue);
	}

	@Override
	public Categorie findOne(String[] paraNames, Object[] ParamValues) {
		return dao.findOne(paraNames, ParamValues);
	}

	@Override
	public int findCountBy(String paraName, String ParamValue) {
		return dao.findCountBy(paraName, ParamValue);
	}

}
