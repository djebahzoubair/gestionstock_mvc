package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.LigneVente;

public interface ILigneVenteService {
	
public LigneVente save (LigneVente entity);
	
	public LigneVente update (LigneVente entity);
	
	public List<LigneVente> selectAll();
	
	public List<LigneVente> selectAll(String sortField, String sort);
	
	public LigneVente getById(Long id);
	
	public void remove(Long id);
	
	public LigneVente findOne(String paraName, Object ParamValue );
	
	public LigneVente findOne(String paraNames[], Object ParamValues[] );
	
	public int findCountBy(String paraName, String ParamValue );

}
