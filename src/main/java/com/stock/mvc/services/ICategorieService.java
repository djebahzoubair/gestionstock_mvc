package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.Categorie;

public interface ICategorieService {
	
public Categorie save (Categorie entity);
	
	public Categorie update (Categorie entity);
	
	public List<Categorie> selectAll();
	
	public List<Categorie> selectAll(String sortField, String sort);
	
	public Categorie getById(Long id);
	
	public void remove(Long id);
	
	public Categorie findOne(String paraName, Object ParamValue );
	
	public Categorie findOne(String paraNames[], Object ParamValues[] );
	
	public int findCountBy(String paraName, String ParamValue );

}
